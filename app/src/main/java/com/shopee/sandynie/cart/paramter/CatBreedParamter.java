package com.shopee.sandynie.cart.paramter;

public class CatBreedParamter extends BaseParamter {
    private int fetchType;

    private int mAttchBreed = 0;
    private int mPage = 0;
    private int mLimit = 10;
    private String searchName;

    public void setmAttchBreed(int mAttchBreed) {
        this.mAttchBreed = mAttchBreed;
    }

    public void setmPage(int mPage) {
        this.mPage = mPage;
    }

    public void setmLimit(int mLimit) {
        this.mLimit = mLimit;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public int getmAttchBreed() {
        return mAttchBreed;
    }

    public int getmPage() {
        return mPage;
    }

    public int getmLimit() {
        return mLimit;
    }

    public String getSearchName() {
        return searchName;
    }

    public  int getFetchType(){
        return fetchType;
    }

    public void setFetchType(int value){
        fetchType = value;
    }

    public boolean canNext(){
        return true;
    }

    public boolean canPre(){
        return mPage >0;
    }

    public void nextPage(){
        mPage++;
    }

    public void prePage(){
        mPage--;
    }
}
