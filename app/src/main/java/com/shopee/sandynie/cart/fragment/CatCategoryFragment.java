package com.shopee.sandynie.cart.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.adapter.CatCategoryAdapter;
import com.shopee.sandynie.cart.base.BaseFragment;
import com.shopee.sandynie.cart.config.AppConstant;
import com.shopee.sandynie.cart.config.MainActivityFragmentIndex;
import com.shopee.sandynie.cart.config.ModelEvent;
import com.shopee.sandynie.cart.decorate.SpaceItemDecoration;
import com.shopee.sandynie.cart.model.CatCategory;
import com.shopee.sandynie.cart.presenter.CatCategoryPresenter;

import java.util.List;

import butterknife.BindView;

public class CatCategoryFragment extends BaseFragment {

    private static final String TAG = CatCategoryFragment.class.getSimpleName();

    private CatCategoryAdapter mAdapter;
    private CatCategoryPresenter mPresenter;

    @BindView(R.id.cat_category_review)
    RecyclerView mRecyclerView;

    public static final CatCategoryFragment newInstance(int resId) {
        CatCategoryFragment catCategoryFragment = new CatCategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstant.ARFUMENT_RESID, resId);
        catCategoryFragment.setArguments(bundle);
        return catCategoryFragment;
    }


    @Override
    public void initData() {

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new SpaceItemDecoration(0, 30));
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        ((GridLayoutManager) layoutManager).setSmoothScrollbarEnabled(true);
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new CatCategoryAdapter(null);
        mRecyclerView.setAdapter(mAdapter);


        mPresenter = new CatCategoryPresenter();
        mPresenter.onBind(this);
        mPresenter.fetchData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.unBind();
    }

    @Override
    public int getIndex() {
        return MainActivityFragmentIndex.CATEGORYFRAGMENT.ordinal();
    }

    @Override
    public void updateView(Object data, ModelEvent eventType) {
        switch (eventType) {
            case FETCHCATCATEGORY:
                mAdapter.updateData((List<CatCategory>) data);
                break;
        }

    }
}
