package com.shopee.sandynie.cart.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.base.BaseFragment;
import com.shopee.sandynie.cart.config.AppConstant;
import com.shopee.sandynie.cart.config.MainActivityFragmentIndex;
import com.shopee.sandynie.cart.config.ModelEvent;
import com.shopee.sandynie.cart.model.CatVote;
import com.shopee.sandynie.cart.model.CatVoteResponse;
import com.shopee.sandynie.cart.presenter.CatVotePresenter;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;

public class CatVoteFragment extends BaseFragment {

    private static final String TAG = CatVoteFragment.class.getSimpleName();
    private CatVotePresenter mPresenter;


    @BindView(R.id.cat_vote_love)
    ImageButton loveBtn;

    @BindView(R.id.cat_vote_nope)
    ImageButton nopeBtn;

    @BindView(R.id.cat_vote_tv)
    EditText subIdTv;

    @BindView(R.id.cat_vote_img)
    ImageView catImage;


    @Override
    public int getIndex() {
        return MainActivityFragmentIndex.VOTEFRAGMENT.ordinal();
    }

    public String getSubIdTvText() {
        return subIdTv.getText().toString();
    }

    public void editTextclearFocus() {
        subIdTv.clearFocus();
    }


    public static final CatVoteFragment newInstance(int resId) {
        CatVoteFragment catVoteFragment = new CatVoteFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstant.ARFUMENT_RESID, resId);
        catVoteFragment.setArguments(bundle);
        return catVoteFragment;
    }

    @Override
    public void initData() {
        super.initData();

        loveBtn = findViewById(R.id.cat_vote_love);
        nopeBtn = findViewById(R.id.cat_vote_nope);
        catImage = findViewById(R.id.cat_vote_img);
        subIdTv = findViewById(R.id.cat_vote_tv);


        subIdTv.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    im.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        });

        mPresenter = new CatVotePresenter();
        mPresenter.onBind(this);
        mPresenter.fetchData();
        loveBtn.setOnClickListener(mPresenter);
        nopeBtn.setOnClickListener(mPresenter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.unBind();
    }


    @Override
    public void updateView(Object data, ModelEvent eventType) {
        switch (eventType) {
            case FETCHCATVOTE:
                List<CatVote> voteList = (List<CatVote>) data;
                if (voteList != null && voteList.size() > 0) {
                    subIdTv.setText("");
                    CatVote display = voteList.get(0);

                    String url = display.getUrl();
                    Picasso.get().load(url)
                            .resize(375, 400).centerCrop().into(catImage);

                }
                break;
            case CATVOTERESPONSE:
                CatVoteResponse response = (CatVoteResponse) data;
                if (response != null) {
                    showTips(response.getMessage());
                    if (response.isSuccess())
                        mPresenter.fetchData();
                }
                break;
        }
    }
}
