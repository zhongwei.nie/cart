package com.shopee.sandynie.cart.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CatFavouriteDeleteResponse {

    @SerializedName("message")
    @Expose
    private String message;


    private static final String SUCCESSMSG ="SUCCESS";
    public void setMesssage(String messsage) {
        this.message = messsage;
    }

    public String getMesssage() {
        return message;
    }

    public boolean isSuccess(){
        return SUCCESSMSG.equals(message);
    }
}
