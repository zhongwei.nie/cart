package com.shopee.sandynie.cart.listener;

import android.view.View;

import com.shopee.sandynie.cart.model.CatImage;

public interface OnCatImageFavouriteClickListener {

    void OnFavouriteClick(View view, int pos, CatImage data);
}
