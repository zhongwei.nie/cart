package com.shopee.sandynie.cart.config;

import com.shopee.sandynie.cart.R;

public class AppConstant {
    public static final String BREED_NAME ="breedId";
    public static final String ARFUMENT_RESID = "fragment_resid";
    public static final int LOADING_RESID = R.id.loadingprogress;
}
