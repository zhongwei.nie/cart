package com.shopee.sandynie.cart.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.listener.OnCatFavouriteItemDeleteClickListener;
import com.shopee.sandynie.cart.model.CatFavourite;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CatFavouriteAdapter  extends RecyclerView.Adapter<CatFavouriteAdapter.ViewHolder> {

    private static final String TAG = CatFavouriteAdapter.class.getSimpleName();

    private List<CatFavourite> mCatFavouritesList;
    private OnCatFavouriteItemDeleteClickListener mClickListener;

    public CatFavouriteAdapter(List<CatFavourite> catFavourites, OnCatFavouriteItemDeleteClickListener listener){
        mCatFavouritesList = catFavourites;
        mClickListener = listener;
    }


    public void updateData(List<CatFavourite> catFavourites){
        mCatFavouritesList = catFavourites;
        notifyDataSetChanged();
    }

    public void deleteDataByIndex(int pos){
        Log.d(TAG,"delete at "+pos);
        mCatFavouritesList.remove(pos);
        notifyItemRemoved(pos);
    }


    @NonNull
    @Override
    public CatFavouriteAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cat_favourite,viewGroup,false);
        return new CatFavouriteAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CatFavouriteAdapter.ViewHolder holder, int i) {

        CatFavourite data = mCatFavouritesList.get(i);

        holder.catDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,  holder.getAdapterPosition() +"clicked!");
                if (mClickListener != null){
                    int pos = holder.getAdapterPosition();
                    CatFavourite data = mCatFavouritesList.get(pos);
                    mClickListener.OnDeleteClick(v,pos,data);
                }
            }
        });


        String url = data.getImage().getUrl();
        Picasso.get().load(url)
                .resize(375, 400).centerCrop().into(holder.catImage);

        holder.catFavId.setText(data.getId().toString());
        holder.catCreated.setText(data.getCreated_at());
        holder.catSubId.setText(data.getSub_id());
        holder.catImageId.setText(data.getImage_id());

    }
    //
    @Override
    public int getItemCount() {
        return mCatFavouritesList == null ? 0 : mCatFavouritesList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cat_favourite_item_img)
        ImageView catImage;

        @BindView(R.id.cat_favourite_item_favid)
        TextView catFavId;

        @BindView(R.id.cat_favourite_item_imageid)
        TextView catImageId;

        @BindView(R.id.cat_favourite_item_subid)
        TextView catSubId;

        @BindView(R.id.cat_favourite_item_created)
        TextView catCreated;

        @BindView(R.id.cat_favourite_item_delete)
        Button catDeleteBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }

}
