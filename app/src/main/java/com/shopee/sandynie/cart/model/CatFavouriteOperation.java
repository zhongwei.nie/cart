package com.shopee.sandynie.cart.model;

import com.shopee.sandynie.cart.config.AppConfig;

import java.util.List;

import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface CatFavouriteOperation {

    @Headers(AppConfig.CATAPIKEY)
    @GET(AppConfig.CATFAVOURITEURL)
    Observable<List<CatFavourite>> getFavourites(@Query("page") int page,
                                               @Query("limit") int limit,
                                                @Query("sub_id") String sub_id);
    @Headers(AppConfig.CATAPIKEY)
    @DELETE(AppConfig.CATFAVOURITEDELETEURL)
    Observable<CatFavouriteDeleteResponse> deleteFavourite(@Path("favourite_id") int favouriteId);

}
