package com.shopee.sandynie.cart.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CatFavourite {

    @SerializedName("created_at")
    @Expose
    private String created_at;


    @SerializedName("id")
    @Expose
    private Integer id;


    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setImage(CatImage image) {
        this.image = image;
    }

    @SerializedName("image_id")
    @Expose
    private String image_id;


    @SerializedName("sub_id")
    @Expose
    private String sub_id;


    public String getCreated_at() {
        return created_at;
    }

    public Integer getId() {
        return id;
    }

    public String getImage_id() {
        return image_id;
    }

    public String getSub_id() {
        return sub_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public CatImage getImage() {
        return image;
    }

    @SerializedName("user_id")
    @Expose
    private String user_id;


    @SerializedName("image")
    @Expose
    private CatImage image;
}
