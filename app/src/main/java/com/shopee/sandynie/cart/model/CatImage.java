package com.shopee.sandynie.cart.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CatImage {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("height")
    @Expose
    private Integer height;

    public void setId(String id) {
        this.id = id;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCategory() {
        if (categories != null && categories.size() > 0) {
            return categories.get(0).getName();
        }
        return "";
    }

    public void setCategories(List<CatImageCategory> categories) {
        this.categories = categories;
    }

    @SerializedName("width")
    @Expose
    private Integer width;

    public String getId() {
        return id;
    }

    public Integer getHeight() {
        return height;
    }

    public Integer getWidth() {
        return width;
    }

    public String getUrl() {
        return url;
    }

    public List<CatImageCategory> getCategories() {
        return categories;
    }

    @SerializedName("url")
    @Expose
    private String url;


    @SerializedName("categories")
    @Expose
    private List<CatImageCategory> categories = null;


}
