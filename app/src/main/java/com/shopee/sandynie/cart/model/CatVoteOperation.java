package com.shopee.sandynie.cart.model;

import com.shopee.sandynie.cart.config.AppConfig;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

public interface CatVoteOperation {


    @Headers(AppConfig.CATAPIKEY)
    @GET(AppConfig.CATVOTELOADURL)
    Observable<List<CatVote>> getRandomVoteImage();


    @Headers(AppConfig.CATAPIKEY)
    @POST(AppConfig.CATVOTESENDURL)
    Observable<CatVoteResponse> sendVoteRequest(@Body RequestBody requestBody);
}
