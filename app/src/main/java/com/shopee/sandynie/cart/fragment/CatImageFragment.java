package com.shopee.sandynie.cart.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.adapter.CatImageAdapter;
import com.shopee.sandynie.cart.base.BaseFragment;
import com.shopee.sandynie.cart.config.AppConstant;
import com.shopee.sandynie.cart.config.MainActivityFragmentIndex;
import com.shopee.sandynie.cart.config.ModelEvent;
import com.shopee.sandynie.cart.model.CatImage;
import com.shopee.sandynie.cart.model.CatImageFavouriteResponse;
import com.shopee.sandynie.cart.presenter.CatImagePresenter;

import java.util.List;

import butterknife.BindView;

public class CatImageFragment extends BaseFragment  {

    private static final String TAG = CatImageFragment.class.getSimpleName();

    private CatImageAdapter mAdapter;
    private CatImagePresenter mPresenter;

    @BindView(R.id.cat_image_review)
    RecyclerView mRecyclerView;

    @BindView(R.id.cat_image_limit)
    Spinner mLimitSpinner;

    @BindView(R.id.cat_image_order)
    Spinner mOrderSpinner;

    @BindView(R.id.cat_image_next)
    Button mNextBtn;

    @BindView(R.id.cat_image_pre)
    Button mPreBtn;


    public static final CatImageFragment newInstance(int resId){
        CatImageFragment catImageFragment = new CatImageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstant.ARFUMENT_RESID,resId);
        catImageFragment.setArguments(bundle);
        return catImageFragment;
    }



    public void updateNextPreStatus(){
        mNextBtn.setEnabled(mPresenter.getParamter().canNext());
        mPreBtn.setEnabled(mPresenter.getParamter().canPre());
    }

    @Override
    public void initData(){

        Activity baseActivity = getActivity();
        ArrayAdapter<CharSequence> adapterLimit = ArrayAdapter.createFromResource(baseActivity,
                R.array.cat_image_limit, android.R.layout.simple_spinner_item);
        adapterLimit.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mLimitSpinner.setAdapter(adapterLimit);


        ArrayAdapter<CharSequence> adapterOrder = ArrayAdapter.createFromResource(baseActivity,
                R.array.cat_image_order, android.R.layout.simple_spinner_item);
        adapterOrder.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mOrderSpinner.setAdapter(adapterOrder);




        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        ((GridLayoutManager) layoutManager).setSmoothScrollbarEnabled(true);
        mRecyclerView.setLayoutManager(layoutManager);


        mPresenter = new CatImagePresenter();
        mPresenter.onBind(this);


        mLimitSpinner.setOnItemSelectedListener(mPresenter);
        mOrderSpinner.setOnItemSelectedListener(mPresenter);
        mPreBtn.setOnClickListener(mPresenter);
        mNextBtn.setOnClickListener(mPresenter);
        mAdapter = new CatImageAdapter(null,mPresenter);
        mRecyclerView.setAdapter(mAdapter);


        mPresenter.fetchData();


    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mPresenter.unBind();
    }

    @Override
    public int getIndex(){
        return MainActivityFragmentIndex.IMAGEFRAGMENT.ordinal();
    }

    @Override
    public void updateView(Object data, ModelEvent eventType) {

        switch (eventType){
            case FETCHCATIMAGES:
                // todo
                mAdapter.updateData((List<CatImage>)data);
                mAdapter.notifyDataSetChanged();
                updateNextPreStatus();
                break;
            case FAVOURITERESPONSE:
                CatImageFavouriteResponse response = (CatImageFavouriteResponse)data;
                showTips(response.getMesssage());
                break;
        }


    }


}
