package com.shopee.sandynie.cart.presenter;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.base.BasePresenter;
import com.shopee.sandynie.cart.config.ModelEvent;
import com.shopee.sandynie.cart.listener.OnCatImageFavouriteClickListener;
import com.shopee.sandynie.cart.model.CatImage;
import com.shopee.sandynie.cart.model.CatImageFavouriteResponse;
import com.shopee.sandynie.cart.model.CatImageModelService;
import com.shopee.sandynie.cart.model.CatMainModelService;
import com.shopee.sandynie.cart.paramter.CatImageParamter;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CatImagePresenter extends BasePresenter implements AdapterView.OnItemSelectedListener, View.OnClickListener, OnCatImageFavouriteClickListener {

    public CatImageParamter getParamter() {
        return paramter;
    }

    private static final String TAG = CatImagePresenter.class.getSimpleName();

    private CatImageParamter paramter = new CatImageParamter();


    public void requestFavourite() {
        mView.showProgress();
        CatImageModelService.getInstance().sendFavouriteImage(paramter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CatImageFavouriteResponse>() {
                    @Override
                    public void onCompleted() {
                        mView.dismissProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissProgress();
                        Log.d(TAG, "ON ERROR");
                    }

                    @Override
                    public void onNext(CatImageFavouriteResponse obj) {
                        Log.d(TAG, "the requestback!");
                        if (obj.isSuccess())
                            CatMainModelService.getInstance().setFavouriteChanged(true);
                        mView.updateView(obj, ModelEvent.FAVOURITERESPONSE);
                    }
                });
    }


    public void fetchData() {
        mView.showProgress();
        CatImageModelService.getInstance().getPublicCatImage(paramter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CatImage>>() {
                    @Override
                    public void onCompleted() {
                        mView.dismissProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissProgress();
                        Log.d(TAG, "ON ERROR");
                    }

                    @Override
                    public void onNext(List<CatImage> obj) {

                        mView.updateView(obj, ModelEvent.FETCHCATIMAGES);
                    }
                });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.cat_image_next:
                getParamter().nextPage();
                fetchData();
                break;
            case R.id.cat_image_pre:
                getParamter().prePage();
                fetchData();
                break;
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.cat_image_limit:
                String limitValue = mView.getContext().getResources().getStringArray(R.array.cat_image_limit)[position];
                int currentValue = Integer.parseInt(limitValue);
                if (currentValue != getParamter().getLimit()) {
                    getParamter().setLimit(Integer.parseInt(limitValue));
                    fetchData();
                }
                break;
            case R.id.cat_image_order:
                String orderValue = mView.getContext().getResources().getStringArray(R.array.cat_image_order)[position];
                if (!orderValue.equals(getParamter().getOrder())) {
                    getParamter().setOrder(orderValue);
                    fetchData();
                }
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void OnFavouriteClick(View view, int pos, CatImage data) {
        String selectImageId = data.getId();
        getParamter().setId(selectImageId);
        requestFavourite();

    }
}
