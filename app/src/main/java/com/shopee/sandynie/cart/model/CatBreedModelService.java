package com.shopee.sandynie.cart.model;

import com.shopee.sandynie.cart.base.BaseModelService;
import com.shopee.sandynie.cart.config.AppConfig;
import com.shopee.sandynie.cart.paramter.CatBreedParamter;

import java.util.List;

import rx.Observable;

public class CatBreedModelService extends BaseModelService {


    private static CatBreedModelService mInstance = new CatBreedModelService();

    private CatBreedModelService(){

    }
    public static CatBreedModelService getInstance(){
        return mInstance;
    }


    public Observable<List<CatBreed>> getCatBreedsList(CatBreedParamter paramter) {
        buildConnection(AppConfig.LISTCATBREEDSURL);
        return mRetrofit.create(CatBreedsOperation.class).getCatBreedsList(paramter.getmAttchBreed(),
                paramter.getmPage(), paramter.getmLimit());
    }


    public Observable<List<CatBreed>> searchBreedsByName(CatBreedParamter paramter){
        buildConnection(AppConfig.SEARCHBREEDSURL);
        return mRetrofit.create(CatBreedsOperation.class).searchBreedsByName(paramter.getSearchName());
    }

}
