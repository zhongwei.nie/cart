package com.shopee.sandynie.cart.base;

import com.shopee.sandynie.cart.config.AppConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class BaseModelService {

    protected   Retrofit mRetrofit;
    protected OkHttpClient mOkHttpClient;

    protected void buildConnection(String url){
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        mOkHttpClient = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build();

        mRetrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create()).client(mOkHttpClient)
                .baseUrl(url).build();

    }
}
