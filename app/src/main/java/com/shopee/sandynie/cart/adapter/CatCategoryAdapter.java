package com.shopee.sandynie.cart.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.model.CatCategory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CatCategoryAdapter extends RecyclerView.Adapter<CatCategoryAdapter.ViewHolder> {

    private static final String TAG = CatBreedAdapter.class.getSimpleName();

    private List<CatCategory> mCatCategoryList;

    public CatCategoryAdapter(List<CatCategory> catCategoryList){
        mCatCategoryList = catCategoryList;
    }


    public void updateData(List<CatCategory> catCategories){
        mCatCategoryList = catCategories;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CatCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cat_category,viewGroup,false);
        return new CatCategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CatCategoryAdapter.ViewHolder holder, int i) {


        holder.mCategory.setText(mCatCategoryList.get(i).getName());

        //ADD FAVORITES

    }
    //
    @Override
    public int getItemCount() {
        return mCatCategoryList == null ? 0 : mCatCategoryList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cat_category)
        TextView mCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
