package com.shopee.sandynie.cart.listener;

import android.view.View;

import com.shopee.sandynie.cart.model.CatBreed;
import com.shopee.sandynie.cart.model.CatFavourite;

public interface OnCatFavouriteItemDeleteClickListener {

    void OnDeleteClick(View view, int pos, CatFavourite data);

}
