package com.shopee.sandynie.cart.presenter;

import android.util.Log;
import android.view.View;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.base.BasePresenter;
import com.shopee.sandynie.cart.config.ModelEvent;
import com.shopee.sandynie.cart.listener.OnCatFavouriteItemDeleteClickListener;
import com.shopee.sandynie.cart.model.CatFavourite;
import com.shopee.sandynie.cart.model.CatFavouriteDeleteResponse;
import com.shopee.sandynie.cart.model.CatFavouriteModelService;
import com.shopee.sandynie.cart.model.CatMainModelService;
import com.shopee.sandynie.cart.paramter.CatFavouriteParamter;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CatFavouritePresenter extends BasePresenter implements View.OnClickListener, OnCatFavouriteItemDeleteClickListener {
    public CatFavouriteParamter getParamter() {
        return paramter;
    }

    private static final String TAG = CatFavouritePresenter.class.getSimpleName();

    private CatFavouriteParamter paramter = new CatFavouriteParamter();

    public void requestDeleteFavourite(){
        mView.showProgress();
        CatFavouriteModelService.getInstance().deleteFavourite(paramter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CatFavouriteDeleteResponse>() {
                    @Override
                    public void onCompleted() {
                        mView.dismissProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissProgress();
                        Log.d(TAG, "ON ERROR");
                    }

                    @Override
                    public void onNext(CatFavouriteDeleteResponse obj) {
                        mView.updateView(obj, ModelEvent.FAVOURITEDELETERESPONSE);
                    }
                });
    }

    public void fetchData() {
        mView.showProgress();
        CatFavouriteModelService.getInstance().getFavourites(paramter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CatFavourite>>() {
                    @Override
                    public void onCompleted() {
                        mView.dismissProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissProgress();
                        Log.d(TAG, "ON ERROR");
                    }

                    @Override
                    public void onNext(List<CatFavourite> obj) {
                        resetFavouriteChanged();
                        mView.updateView(obj, ModelEvent.FETCHCATFAVOURITE);
                    }
                });
    }



    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.cat_favourite_pre:
                getParamter().prePage();
                fetchData();
                break;
            case R.id.cat_favourite_next:
                getParamter().nextPage();
                fetchData();
                break;
        }
    }

    @Override
    public void OnDeleteClick(View view, int pos, CatFavourite data) {
        getParamter().setFavouriteId(data.getId());

        getParamter().setDeleteIndex(pos);
        requestDeleteFavourite();
        Log.d(TAG,"delete clicked! at index :" +pos);
    }

    public boolean isFavouriteChanged(){
        return CatMainModelService.getInstance().isFavouriteChanged();
    }

    public void resetFavouriteChanged(){
        CatMainModelService.getInstance().setFavouriteChanged(false);
    }
}




