package com.shopee.sandynie.cart.fragment;


import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.adapter.CatBreedAdapter;
import com.shopee.sandynie.cart.base.BaseFragment;
import com.shopee.sandynie.cart.config.AppConstant;
import com.shopee.sandynie.cart.config.MainActivityFragmentIndex;
import com.shopee.sandynie.cart.config.ModelEvent;
import com.shopee.sandynie.cart.model.CatBreed;
import com.shopee.sandynie.cart.presenter.CatBreedPresenter;

import java.util.List;

import butterknife.BindView;

public class CatBreedFragment extends BaseFragment {

    private static final String TAG = CatBreedFragment.class.getSimpleName();

    private CatBreedAdapter mAdapter;
    private CatBreedPresenter mPresenter;

    @BindView(R.id.cat_breed_review)
    RecyclerView mRecyclerView;

    @BindView(R.id.cat_breed_pre)
    Button mPreBtn;

    @BindView(R.id.cat_breed_next)
    Button mNextBtn;

    public static final CatBreedFragment newInstance(int resId) {
        CatBreedFragment catBreedFragment = new CatBreedFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstant.ARFUMENT_RESID, resId);
        catBreedFragment.setArguments(bundle);
        return catBreedFragment;
    }


    public void updateNextPreStatus() {
        mNextBtn.setEnabled(mPresenter.getParamater().canNext());
        mPreBtn.setEnabled(mPresenter.getParamater().canPre());
    }


    @Override
    public void initData() {

        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        ((GridLayoutManager) layoutManager).setSmoothScrollbarEnabled(true);
        mRecyclerView.setLayoutManager(layoutManager);

        mPresenter = new CatBreedPresenter();

        mAdapter = new CatBreedAdapter(null, mPresenter);
        mRecyclerView.setAdapter(mAdapter);

        mPreBtn.setOnClickListener(mPresenter);
        mNextBtn.setOnClickListener(mPresenter);

        mPresenter.onBind(this);
        mPresenter.getParamater().setFetchType(0);
        mPresenter.fetchData();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.unBind();
    }

    @Override
    public int getIndex() {
        return MainActivityFragmentIndex.BREEDFRAGMENT.ordinal();
    }


    @Override
    public void updateView(Object data, ModelEvent eventType) {
        switch (eventType) {
            case FETCHCATBREED:
                mAdapter.updateData((List<CatBreed>) data);
                updateNextPreStatus();
                break;
        }

    }


}
