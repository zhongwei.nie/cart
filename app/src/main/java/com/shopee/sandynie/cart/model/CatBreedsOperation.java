package com.shopee.sandynie.cart.model;

import com.shopee.sandynie.cart.config.AppConfig;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;

public interface CatBreedsOperation {

    @Headers(AppConfig.CATAPIKEY)
    @GET(AppConfig.LISTCATBREEDSURL)
    Observable<List<CatBreed>> getCatBreedsList(@Query("attch_bread") int attah_bread,
                                                @Query("page") int page,
                                                @Query("limit") int limit);

    @Headers(AppConfig.CATAPIKEY)
    @GET(AppConfig.SEARCHBREEDSURL)
    Observable<List<CatBreed>> searchBreedsByName(@Query("q") String name);
}
