package com.shopee.sandynie.cart.presenter;

import android.util.Log;
import android.view.View;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.base.BasePresenter;
import com.shopee.sandynie.cart.config.ModelEvent;
import com.shopee.sandynie.cart.fragment.CatVoteFragment;
import com.shopee.sandynie.cart.model.CatVote;
import com.shopee.sandynie.cart.model.CatVoteResponse;
import com.shopee.sandynie.cart.model.CatVoteModelService;
import com.shopee.sandynie.cart.paramter.CatVoteParamter;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CatVotePresenter extends BasePresenter  implements View.OnClickListener{

    private static final String TAG = CatVotePresenter.class.getSimpleName();
    private CatVoteParamter paramter = new CatVoteParamter();
    private CatVote mCurrentVoteInfo;


    public CatVoteParamter getParamter() {
        return paramter;
    }

    public void setParamter(CatVoteParamter paramter) {
        this.paramter = paramter;
    }



    public void requestVote(boolean love, String subId){
        if(mCurrentVoteInfo != null) {
            paramter.set(mCurrentVoteInfo.getId(), love, subId);

            mView.showProgress();
            CatVoteModelService.getInstance().sendCatVote(paramter)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<CatVoteResponse>() {
                        @Override
                        public void onCompleted() {
                            mView.dismissProgress();
                        }

                        @Override
                        public void onError(Throwable e) {
                            mView.dismissProgress();
                            Log.d(TAG, "ON ERROR");
                        }

                        @Override
                        public void onNext(CatVoteResponse obj) {
                            mView.updateView(obj,ModelEvent.CATVOTERESPONSE);
                        }
                    });
        }
    }

    public void fetchData() {
        mView.showProgress();
        CatVoteModelService.getInstance().getVoteRandomImage(paramter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CatVote>>() {
                    @Override
                    public void onCompleted() {
                        mView.dismissProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dismissProgress();
                        Log.d(TAG, "ON ERROR");
                    }

                    @Override
                    public void onNext(List<CatVote> obj) {
                        List<CatVote>  voteList = (List<CatVote>)obj;
                        if (voteList != null && voteList.size() >0) {
                            mView.updateView(voteList, ModelEvent.FETCHCATVOTE);
                            mCurrentVoteInfo = voteList.get(0);
                        }
                    }
                });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        CatVoteFragment catVoteFragment = (CatVoteFragment)mView;
        switch (id){
            case R.id.cat_vote_love:
                Log.d(TAG,"cat_vote_love clicked!");
                requestVote(true,catVoteFragment.getSubIdTvText());
                catVoteFragment.editTextclearFocus();
                break;
            case R.id.cat_vote_nope:
                Log.d(TAG,"cat_vote_nope clicked");
                requestVote(false,catVoteFragment.getSubIdTvText());
                catVoteFragment.editTextclearFocus();
                break;
        }
    }

}
