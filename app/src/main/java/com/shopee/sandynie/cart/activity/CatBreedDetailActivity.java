package com.shopee.sandynie.cart.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.base.BaseAcitivity;
import com.shopee.sandynie.cart.config.AppConstant;
import com.shopee.sandynie.cart.config.ModelEvent;
import com.shopee.sandynie.cart.model.CatBreed;
import com.shopee.sandynie.cart.presenter.CatBreedPresenter;

import java.util.List;

import butterknife.BindView;

public class CatBreedDetailActivity extends BaseAcitivity {

    private static final String TAG = CatBreedDetailActivity.class.getSimpleName();

    @BindView(R.id.adaptability)
    TextView adaptability;

    @BindView(R.id.affection_level)
    TextView affection_level;

    @BindView(R.id.alt_names)
    TextView alt_names;

    @BindView(R.id.cfa_url)
    TextView cfa_url;

    @BindView(R.id.child_friendly)
    TextView child_friendly;

    @BindView(R.id.country_code)
    TextView country_code;

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.dog_friendly)
    TextView dog_friendly;

    @BindView(R.id.energy_level)
    TextView energy_level;

    @BindView(R.id.name)
    TextView name;


    @BindView(R.id.temperament)
    TextView temperament;

    @BindView(R.id.vcahospitals_url)
    TextView vcahospitals_url;

    @BindView(R.id.vetstreet_url)
    TextView vetstreet_url;

    @BindView(R.id.wikipedia_url)
    TextView wikipedia_url;

    private CatBreedPresenter mPresenter;
    private String breedId;



    @Override
    public void updateView(Object data, ModelEvent eventType) {
        List<CatBreed>  catList = (List<CatBreed>)data;
        if (catList != null && catList.size() >0){
            CatBreed cat = catList.get(0);
            adaptability.setText(cat.getAdaptability().toString());
            affection_level.setText(cat.getAffection_level().toString());
            alt_names.setText(cat.getAlt_names());
            cfa_url.setText(cat.getCfa_url());
            child_friendly.setText(cat.getChild_friendly().toString());
            country_code.setText(cat.getCountry_code().toString());
            description.setText(cat.getDescription());
            dog_friendly.setText(cat.getDog_friendly().toString());
            energy_level.setText(cat.getEnergy_level().toString());
            name.setText(cat.getName());
            temperament.setText(cat.getTemperament());
            vcahospitals_url.setText(cat.getVcahospitals_url());
            vetstreet_url.setText(cat.getVetstreet_url());
            wikipedia_url.setText(cat.getWikipedia_url());
        }
    }


    @Override
    public void initData() {
        setContentView(R.layout.activity_catbreed_detail);
        Intent intent = getIntent();
        String result=intent.getStringExtra(AppConstant.BREED_NAME);
        mPresenter = new CatBreedPresenter();
        mPresenter.onBind(this);
        mPresenter.getParamater().setSearchName(result);
        mPresenter.fetchData();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unBind();
    }
}
