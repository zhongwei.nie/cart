package com.shopee.sandynie.cart.base;

import android.content.Context;
import android.content.Intent;

import com.shopee.sandynie.cart.config.ModelEvent;

public interface BaseView{

    void showProgress();
    void dismissProgress();
    void showTips(String content);
    void updateView(Object data, ModelEvent eventType);
    void initData();
    Context getContext();
    void forceUpdate();
}
