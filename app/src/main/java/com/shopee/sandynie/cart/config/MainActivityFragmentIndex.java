package com.shopee.sandynie.cart.config;

import android.util.SparseArray;

import com.shopee.sandynie.cart.base.BaseFragment;

public enum  MainActivityFragmentIndex {

    BREEDFRAGMENT,
    IMAGEFRAGMENT,
    VOTEFRAGMENT,
    FAVOURITEFRAGMENT,
    CATEGORYFRAGMENT, ;

    private static SparseArray<BaseFragment> cacheMap = new SparseArray<>();


    public static BaseFragment getFragment(int key) {
        return cacheMap.get(key);
    }

    public static void putFragment(int key, BaseFragment fragment){
        cacheMap.put(key,fragment);
    }

    public static void clear(){
        cacheMap.clear();
    }

}
