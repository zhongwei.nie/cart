package com.shopee.sandynie.cart.activity;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.base.BaseFragment;
import com.shopee.sandynie.cart.config.MainActivityFragmentIndex;
import com.shopee.sandynie.cart.fragment.CatBreedFragment;
import com.shopee.sandynie.cart.fragment.CatCategoryFragment;
import com.shopee.sandynie.cart.fragment.CatFavouriteFragment;
import com.shopee.sandynie.cart.fragment.CatImageFragment;
import com.shopee.sandynie.cart.fragment.CatVoteFragment;
import com.shopee.sandynie.cart.presenter.MainPresenter;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = MainActivity.class.getSimpleName();

    BottomNavigationView buttonNavigationView;

    private BaseFragment currentFragment;
    private MainPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonNavigationView = findViewById(R.id.mainbottom_navigator);
        buttonNavigationView.setOnNavigationItemSelectedListener(this);

        FragmentManager manager = getSupportFragmentManager();
        CatBreedFragment fragment =  CatBreedFragment.newInstance(R.layout.fragment_cat_breed);

        MainActivityFragmentIndex.putFragment(fragment.getIndex(),fragment);
        currentFragment = fragment;
        mPresenter = new MainPresenter();
        buttonNavigationView = findViewById(R.id.mainbottom_navigator);
        buttonNavigationView.setOnNavigationItemSelectedListener(this);

        manager.beginTransaction().add(R.id.mainframe_container, fragment).commit();
    }

    private BaseFragment createFragmentByIndex(int index){
        BaseFragment fragment = null;
        switch (index){
            case 0:
                fragment =  CatBreedFragment.newInstance(R.layout.fragment_cat_breed);
                break;
            case 1:
                fragment = CatImageFragment.newInstance(R.layout.fragment_cat_image);
                break;
            case 2:
                fragment =   CatVoteFragment.newInstance(R.layout.fragment_cat_vote);
                break;
            case 3:
                fragment = CatFavouriteFragment.newInstance(R.layout.fragment_cat_favourite);
                break;
            case 4:
                fragment =  CatCategoryFragment.newInstance(R.layout.fragment_cat_category);
        }
        return fragment;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int index = 0;
        switch (menuItem.getItemId()){
            case R.id.navigation_cat_breed:
                index = 0;
                break;
            case R.id.navigation_cat_images:
                index = 1;
                break;
            case R.id.navigation_cat_vote:
                index = 2;
                break;
            case R.id.navigation_cat_favourite:
                index = 3;
                break;
            case R.id.navigation_cat_category:
                index = 4;
                break;
        }

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction =  manager.beginTransaction();
        if (currentFragment != null){
            transaction.hide(currentFragment);
        }

        BaseFragment fragment;
        if ((fragment = MainActivityFragmentIndex.getFragment(index)) == null ){
            fragment =createFragmentByIndex(index);
            int i = fragment.getIndex();
            MainActivityFragmentIndex.putFragment(i,fragment);
        }

        if (!fragment.isAdded()){
            transaction.add(R.id.mainframe_container, fragment);
        }else{
            transaction.show(fragment);
            fragment.forceUpdate();

        }
        currentFragment = fragment;
        transaction.addToBackStack(null);
        transaction.commit();
        return true;
    }

}
