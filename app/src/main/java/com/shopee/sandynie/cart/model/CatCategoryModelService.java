package com.shopee.sandynie.cart.model;

import com.shopee.sandynie.cart.base.BaseModelService;
import com.shopee.sandynie.cart.config.AppConfig;
import com.shopee.sandynie.cart.paramter.CatCategoryParamter;

import java.util.List;

import rx.Observable;

public class CatCategoryModelService extends BaseModelService {


    private static CatCategoryModelService mInstance = new CatCategoryModelService();

    private CatCategoryModelService(){

    }
    public static CatCategoryModelService getInstance(){
        return mInstance;
    }


    public Observable<List<CatCategory>> getCatCategories(CatCategoryParamter paramter) {
        buildConnection(AppConfig.LISTCATCATEGORYURL);
        return mRetrofit.create(CatCategoryOperation.class).getCatgories(paramter.getmPage(),paramter.getmLimit());
    }

}
