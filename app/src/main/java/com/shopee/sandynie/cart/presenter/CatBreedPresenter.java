package com.shopee.sandynie.cart.presenter;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.activity.CatBreedDetailActivity;
import com.shopee.sandynie.cart.base.BasePresenter;
import com.shopee.sandynie.cart.config.AppConstant;
import com.shopee.sandynie.cart.config.ModelEvent;
import com.shopee.sandynie.cart.daoutils.CatBreedDaoUtil;
import com.shopee.sandynie.cart.listener.OnCatBreedItemClickListener;
import com.shopee.sandynie.cart.model.CatBreed;
import com.shopee.sandynie.cart.model.CatBreedModelService;
import com.shopee.sandynie.cart.paramter.CatBreedParamter;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CatBreedPresenter extends BasePresenter implements OnCatBreedItemClickListener, View.OnClickListener {

    private static final String TAG = CatBreedPresenter.class.getSimpleName();

    public CatBreedParamter getParamater() {
        return paramater;
    }

    private CatBreedParamter paramater = new CatBreedParamter();


    public void fetchData() {

        switch (paramater.getFetchType()) {
            case 0:

                List<CatBreed> cachedData = CatBreedDaoUtil.getCatBreedsByPage(paramater.getmPage());
                if (cachedData != null){
                    Log.d(TAG,"get the cached data");
                    mView.updateView(cachedData, ModelEvent.FETCHCATBREED);
                }else {
                    Log.d(TAG,"can not get the cached data from the dao");
                    mView.showProgress();
                    CatBreedModelService.getInstance().getCatBreedsList(paramater)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<List<CatBreed>>() {
                                @Override
                                public void onCompleted() {
                                    Log.d(TAG, "onCompleted");
                                    mView.dismissProgress();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    mView.dismissProgress();
                                }

                                @Override
                                public void onNext(List<CatBreed> obj) {
                                    CatBreedDaoUtil.saveCatBreedToDao(paramater.getmPage(),obj);
                                    mView.updateView(obj, ModelEvent.FETCHCATBREED);
                                }
                            });
                }
                break;

            case 1:
                mView.showProgress();
                CatBreedModelService.getInstance().searchBreedsByName(paramater)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<CatBreed>>() {
                            @Override
                            public void onCompleted() {
                                mView.dismissProgress();
                            }

                            @Override
                            public void onError(Throwable e) {
                                mView.dismissProgress();
                                Log.d(TAG, "ON ERROR");
                            }

                            @Override
                            public void onNext(List<CatBreed> obj) {

                                mView.updateView(obj,ModelEvent.FETCHCATBREEDDETAIL);
                            }
                        });

                break;
        }
    }



    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.cat_breed_next:
                getParamater().nextPage();
                fetchData();
                break;
            case R.id.cat_breed_pre:
                getParamater().prePage();
                fetchData();
                break;
        }
    }


    @Override
    public void OnItemClick(View view, int pos, CatBreed data) {
        String breedName = data.getName();
        Intent intent = new Intent(mView.getContext(), CatBreedDetailActivity.class);
        intent.putExtra(AppConstant.BREED_NAME,breedName);
        mView.getContext().startActivity(intent);

    }

}
