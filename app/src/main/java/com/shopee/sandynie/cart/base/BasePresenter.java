package com.shopee.sandynie.cart.base;

import com.google.gson.Gson;

public abstract class BasePresenter {


    protected  BaseView mView;

    public void onBind(BaseView  view){
        mView = view;
    }

    public void unBind(){
        mView = null;
    }

    public abstract void fetchData();


}
