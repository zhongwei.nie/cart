package com.shopee.sandynie.cart.model;

import com.shopee.sandynie.cart.base.BaseModelService;
import com.shopee.sandynie.cart.config.AppConfig;
import com.shopee.sandynie.cart.paramter.CatImageParamter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;

public class CatImageModelService extends BaseModelService {

    private static CatImageModelService mInstance = new CatImageModelService();

    private CatImageModelService() {

    }

    public static CatImageModelService getInstance() {
        return mInstance;
    }


    public Observable<List<CatImage>> getPublicCatImage(CatImageParamter paramter) {
        buildConnection(AppConfig.LISTALLIMAGEURL);
        return mRetrofit.create(CatImageOperation.class).
                getPublicImage(paramter.getSize(), paramter.getMime_types(),
                        paramter.getOrder(), paramter.getLimit(),
                        paramter.getPage(), paramter.getCategory_ids(),
                        paramter.getFormat(), paramter.getBreed_id());
    }

    public Observable<CatImageFavouriteResponse> sendFavouriteImage(CatImageParamter paramter) {
        buildConnection(AppConfig.CATFAVOURITEURL);
        JSONObject requestData = new JSONObject();
        try {
            requestData.put("image_id", paramter.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), requestData.toString());
        return mRetrofit.create(CatImageOperation.class).sentFarvourite(requestBody);
    }
}
