package com.shopee.sandynie.cart.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.listener.OnCatImageFavouriteClickListener;
import com.shopee.sandynie.cart.model.CatImage;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CatImageAdapter extends RecyclerView.Adapter<CatImageAdapter.ViewHolder>  {


    private static final String TAG = CatImageAdapter.class.getSimpleName();

    private List<CatImage> mCatImageList;
    private OnCatImageFavouriteClickListener mListener;

    public CatImageAdapter(List<CatImage> catImages,OnCatImageFavouriteClickListener listener){
        mCatImageList = catImages;
        mListener = listener;
    }

    public void updateData(List<CatImage> catImageList){
        mCatImageList = catImageList;
    }

    @NonNull
    @Override
    public CatImageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cat_image,viewGroup,false);
        return new CatImageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CatImageAdapter.ViewHolder holder, int i) {


        holder.categoryTv.setText(mCatImageList.get(i).getCategory());
        holder.catFavouriteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"come into favourite click");
                int pos = holder.getAdapterPosition();
                CatImage data = mCatImageList.get(pos);
                if (mListener != null){
                    mListener.OnFavouriteClick(v,pos,data);
                }
            }
        });


        String url = mCatImageList.get(i).getUrl();
        Picasso.get().load(url)
                .resize(375, 400).centerCrop().into(holder.catImage);

    }
    //
    @Override
    public int getItemCount() {
       return mCatImageList == null ? 0 : mCatImageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cat_image_item_category)
        TextView categoryTv;

        @BindView(R.id.cat_image_item_img)
        ImageView catImage;

        @BindView(R.id.cat_image_item_favourite)
        Button catFavouriteBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
