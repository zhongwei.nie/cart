package com.shopee.sandynie.cart.model;

import com.shopee.sandynie.cart.base.BaseModelService;
import com.shopee.sandynie.cart.config.AppConfig;
import com.shopee.sandynie.cart.paramter.CatFavouriteParamter;

import java.util.List;

import rx.Observable;

public class CatFavouriteModelService extends BaseModelService {


    private static CatFavouriteModelService mInstance = new CatFavouriteModelService();

    private CatFavouriteModelService(){

    }
    public static CatFavouriteModelService getInstance(){
        return mInstance;
    }


    public Observable<List<CatFavourite>> getFavourites(CatFavouriteParamter paramter) {
        buildConnection(AppConfig.CATFAVOURITEURL);
        return mRetrofit.create(CatFavouriteOperation.class).
                getFavourites( paramter.getPage(),paramter.getLimit(),paramter.getSubId());
    }


    public Observable<CatFavouriteDeleteResponse> deleteFavourite(CatFavouriteParamter paramter){
        buildConnection(AppConfig.CATFAVOURITEDELETEURL);
        return mRetrofit.create(CatFavouriteOperation.class).deleteFavourite(paramter.getFavouriteId());
    }


}
