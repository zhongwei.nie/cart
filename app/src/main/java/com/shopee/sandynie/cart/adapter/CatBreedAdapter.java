package com.shopee.sandynie.cart.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.listener.OnCatBreedItemClickListener;
import com.shopee.sandynie.cart.model.CatBreed;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CatBreedAdapter extends RecyclerView.Adapter<CatBreedAdapter.ViewHolder> {

    private static final String TAG = CatBreedAdapter.class.getSimpleName();

    private List<CatBreed> mCatBreedList;
    private OnCatBreedItemClickListener mListener;
    public CatBreedAdapter(List<CatBreed> catBreedList, OnCatBreedItemClickListener listener){
        mCatBreedList = catBreedList;
        mListener = listener;
    }

    public void updateData(List<CatBreed> catBreedList){
        mCatBreedList = catBreedList;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cat_breed,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {


        holder.mCatBreedBtn.setText(mCatBreedList.get(i).getName());
        holder.mCatBreedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = holder.getAdapterPosition();
                CatBreed catBreed = mCatBreedList.get(pos);
                if (mListener != null){
                    mListener.OnItemClick(v,pos,catBreed);
                }
            }
        });

        //ADD FAVORITES

    }
    //
    @Override
    public int getItemCount() {
       return mCatBreedList == null ? 0 : mCatBreedList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.catbreedbtn)
        Button mCatBreedBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
