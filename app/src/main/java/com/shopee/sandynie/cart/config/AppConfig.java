package com.shopee.sandynie.cart.config;

public class AppConfig {

    public static final String DBNAME = "cat-db";

    public static final String LISTCATBREEDSURL ="https://api.thecatapi.com/v1/breeds/";
    public static final String SEARCHBREEDSURL = "https://api.thecatapi.com/v1/breeds/search/";

    public static final String LISTCATCATEGORYURL = "https://api.thecatapi.com/v1/categories/";

    public static final String LISTALLIMAGEURL = "https://api.thecatapi.com/v1/images/search/";

    public static final String CATVOTELOADURL = "https://api.thecatapi.com/v1/images/search/";

    public static final String CATVOTESENDURL = "https://api.thecatapi.com/v1/votes/";

    public static final String CATFAVOURITEURL = "https://api.thecatapi.com/v1/favourites/";

    public static final String CATFAVOURITEDELETEURL = "https://api.thecatapi.com/v1/favourites/{favourite_id}/";

    public static final String CATAPIKEY = "x-api-key: 24c39656-6e14-4141-b17f-16d4b9058737";
}
