package com.shopee.sandynie.cart.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.adapter.CatFavouriteAdapter;
import com.shopee.sandynie.cart.base.BaseFragment;
import com.shopee.sandynie.cart.config.AppConstant;
import com.shopee.sandynie.cart.config.MainActivityFragmentIndex;
import com.shopee.sandynie.cart.config.ModelEvent;
import com.shopee.sandynie.cart.model.CatFavourite;
import com.shopee.sandynie.cart.model.CatFavouriteDeleteResponse;
import com.shopee.sandynie.cart.presenter.CatFavouritePresenter;

import java.util.List;

import butterknife.BindView;

public class CatFavouriteFragment extends BaseFragment {
    private static final String TAG = CatFavouriteFragment.class.getSimpleName();


    private CatFavouriteAdapter mAdapter;
    private CatFavouritePresenter mPresenter;

    @BindView(R.id.cat_favourite_review)
    RecyclerView mRecyclerView;

    @BindView(R.id.cat_favourite_pre)
    Button mNextBtn;

    @BindView(R.id.cat_favourite_next)
    Button mPreBtn;

    @Override
    public void forceUpdate() {
        super.forceUpdate();
        Log.d(TAG, "forceUpdate");
        if (mPresenter.isFavouriteChanged())
            mPresenter.fetchData();

    }

    public static final CatFavouriteFragment newInstance(int resId) {
        CatFavouriteFragment catFavouriteFragment = new CatFavouriteFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstant.ARFUMENT_RESID, resId);
        catFavouriteFragment.setArguments(bundle);
        return catFavouriteFragment;
    }


    @Override
    public int getIndex() {
        return MainActivityFragmentIndex.FAVOURITEFRAGMENT.ordinal();
    }

    public void updateNextPreStatus() {
        mNextBtn.setEnabled(mPresenter.getParamter().canNext());
        mPreBtn.setEnabled(mPresenter.getParamter().canPre());
    }

    @Override
    public void initData() {


        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        ((GridLayoutManager) layoutManager).setSmoothScrollbarEnabled(true);
        mRecyclerView.setLayoutManager(layoutManager);

        mPresenter = new CatFavouritePresenter();
        mPresenter.onBind(this);

        mAdapter = new CatFavouriteAdapter(null, mPresenter);
        mRecyclerView.setAdapter(mAdapter);
        mPresenter.fetchData();

        mNextBtn.setOnClickListener(mPresenter);
        mPreBtn.setOnClickListener(mPresenter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.unBind();
        Log.d(TAG, "onDestory");
    }


    @Override
    public void updateView(Object data, ModelEvent eventType) {
        switch (eventType) {
            case FETCHCATFAVOURITE:
                mAdapter.updateData((List<CatFavourite>) data);
                updateNextPreStatus();
                break;
            case FAVOURITEDELETERESPONSE:
                CatFavouriteDeleteResponse response = (CatFavouriteDeleteResponse) data;
                Log.d(TAG, "delete response:" + response.getMesssage());
                if (response != null && response.isSuccess()) {
                    int deleteIndx = mPresenter.getParamter().getDeleteIndex();
                    Log.d(TAG, "update delete at index :" + deleteIndx);
                    mAdapter.deleteDataByIndex(mPresenter.getParamter().getDeleteIndex());
                }
                showTips(response.getMesssage());
                break;
        }
    }


}
