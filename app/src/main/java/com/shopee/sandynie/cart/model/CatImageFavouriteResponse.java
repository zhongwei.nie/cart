package com.shopee.sandynie.cart.model;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CatImageFavouriteResponse {



    @SerializedName("message")
    @Expose
    private String messsage;


    @SerializedName("id")
    @Expose
    private Integer id;

    private static final String SUCESSMSG = "SUCCESS";

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMesssage() {
        return messsage;
    }

    public void setMesssage(String messsage) {
        this.messsage = messsage;
    }


    public boolean isSuccess(){
        Log.d("CatImagePresenter","the msg: "+messsage);
        return SUCESSMSG.equals(messsage);
    }


}
