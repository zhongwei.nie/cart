package com.shopee.sandynie.cart.model;

import com.shopee.sandynie.cart.base.BaseModelService;

public class CatMainModelService extends BaseModelService {

    private static CatMainModelService mInstance = new CatMainModelService();

    private CatMainModelService(){

    }
    public static CatMainModelService getInstance(){
        return mInstance;
    }


    private  boolean favouriteChanged;

    public void setFavouriteChanged(boolean favouriteChanged) {
        this.favouriteChanged = favouriteChanged;
    }

    public boolean isFavouriteChanged() {
        return favouriteChanged;
    }
}
