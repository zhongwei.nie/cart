package com.shopee.sandynie.cart.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CatBreed {
    @SerializedName("adaptability")
    @Expose
    private Integer adaptability;


    @SerializedName("affection_level")
    @Expose
    private Integer affection_level;


    @SerializedName("alt_names")
    @Expose
    private String alt_names;


    @SerializedName("cfa_url")
    @Expose
    private String cfa_url;


    @SerializedName("child_friendly")
    @Expose
    private Integer child_friendly;


    @SerializedName("country_code")
    @Expose
    private String country_code;


    @SerializedName("country_codes")
    @Expose
    private String country_codes;


    @SerializedName("description")
    @Expose
    private String description;


    @SerializedName("dog_friendly")
    @Expose
    private Integer dog_friendly;


    @SerializedName("energy_level")
    @Expose
    private Integer energy_level;


    @SerializedName("experimental")
    @Expose
    private Integer experimental;


    @SerializedName("grooming")
    @Expose
    private Integer grooming;


    @SerializedName("hairless")
    @Expose
    private Integer hairless;


    @SerializedName("health_issues")
    @Expose
    private Integer health_issues;

    @SerializedName("hypoallergenic")
    @Expose
    private Integer hypoallergenic;


    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("indoor")
    @Expose
    private Integer indoor;

    @SerializedName("intelligence")
    @Expose
    private Integer intelligence;

    @SerializedName("lap")
    @Expose
    private Integer lap;

    @SerializedName("life_span")
    @Expose
    private String life_span;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("natural")
    @Expose
    private Integer natural;

    @SerializedName("origin")
    @Expose
    private String origin;

    @SerializedName("rare")
    @Expose
    private Integer rare;


    @SerializedName("rex")
    @Expose
    private Integer rex;


    @SerializedName("shedding_level")
    @Expose
    private Integer shedding_level;

    @SerializedName("short_legs")
    @Expose
    private Integer short_legs;

    @SerializedName("social_needs")
    @Expose
    private Integer social_needs;

    @SerializedName("stranger_friendly")
    @Expose
    private Integer stranger_friendly;

    @SerializedName("suppressed_tail")
    @Expose
    private Integer suppressed_tail;

    @SerializedName("temperament")
    @Expose
    private String temperament;

    @SerializedName("vcahospitals_url")
    @Expose
    private String vcahospitals_url;

    @SerializedName("vetstreet_url")
    @Expose
    private String vetstreet_url;

    @SerializedName("vocalisation")
    @Expose
    private Integer vocalisation;

    @SerializedName("wikipedia_url")
    @Expose
    private String wikipedia_url;

    public void setAdaptability(Integer adaptability) {
        this.adaptability = adaptability;
    }

    public void setAffection_level(Integer affection_level) {
        this.affection_level = affection_level;
    }

    public void setAlt_names(String alt_names) {
        this.alt_names = alt_names;
    }

    public void setCfa_url(String cfa_url) {
        this.cfa_url = cfa_url;
    }

    public void setChild_friendly(Integer child_friendly) {
        this.child_friendly = child_friendly;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public void setCountry_codes(String country_codes) {
        this.country_codes = country_codes;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDog_friendly(Integer dog_friendly) {
        this.dog_friendly = dog_friendly;
    }

    public void setEnergy_level(Integer energy_level) {
        this.energy_level = energy_level;
    }

    public void setExperimental(Integer experimental) {
        this.experimental = experimental;
    }

    public void setGrooming(Integer grooming) {
        this.grooming = grooming;
    }

    public void setHairless(Integer hairless) {
        this.hairless = hairless;
    }

    public void setHealth_issues(Integer health_issues) {
        this.health_issues = health_issues;
    }

    public void setHypoallergenic(Integer hypoallergenic) {
        this.hypoallergenic = hypoallergenic;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIndoor(Integer indoor) {
        this.indoor = indoor;
    }

    public void setIntelligence(Integer intelligence) {
        this.intelligence = intelligence;
    }

    public void setLap(Integer lap) {
        this.lap = lap;
    }

    public void setLife_span(String life_span) {
        this.life_span = life_span;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNatural(Integer natural) {
        this.natural = natural;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setRare(Integer rare) {
        this.rare = rare;
    }

    public void setRex(Integer rex) {
        this.rex = rex;
    }

    public void setShedding_level(Integer shedding_level) {
        this.shedding_level = shedding_level;
    }

    public void setShort_legs(Integer short_legs) {
        this.short_legs = short_legs;
    }

    public void setSocial_needs(Integer social_needs) {
        this.social_needs = social_needs;
    }

    public void setStranger_friendly(Integer stranger_friendly) {
        this.stranger_friendly = stranger_friendly;
    }

    public void setSuppressed_tail(Integer suppressed_tail) {
        this.suppressed_tail = suppressed_tail;
    }

    public void setTemperament(String temperament) {
        this.temperament = temperament;
    }

    public void setVcahospitals_url(String vcahospitals_url) {
        this.vcahospitals_url = vcahospitals_url;
    }

    public void setVetstreet_url(String vetstreet_url) {
        this.vetstreet_url = vetstreet_url;
    }

    public void setVocalisation(Integer vocalisation) {
        this.vocalisation = vocalisation;
    }

    public void setWikipedia_url(String wikipedia_url) {
        this.wikipedia_url = wikipedia_url;
    }

    public Integer getAdaptability() {
        return adaptability;
    }

    public Integer getAffection_level() {
        return affection_level;
    }

    public String getAlt_names() {
        return alt_names;
    }

    public String getCfa_url() {
        return cfa_url;
    }

    public Integer getChild_friendly() {
        return child_friendly;
    }

    public String getCountry_code() {
        return country_code;
    }

    public String getCountry_codes() {
        return country_codes;
    }

    public String getDescription() {
        return description;
    }

    public Integer getDog_friendly() {
        return dog_friendly;
    }

    public Integer getEnergy_level() {
        return energy_level;
    }

    public Integer getExperimental() {
        return experimental;
    }

    public Integer getGrooming() {
        return grooming;
    }

    public Integer getHairless() {
        return hairless;
    }

    public Integer getHealth_issues() {
        return health_issues;
    }

    public Integer getHypoallergenic() {
        return hypoallergenic;
    }

    public String getId() {
        return id;
    }

    public Integer getIndoor() {
        return indoor;
    }

    public Integer getIntelligence() {
        return intelligence;
    }

    public Integer getLap() {
        return lap;
    }

    public String getLife_span() {
        return life_span;
    }

    public String getName() {
        return name;
    }

    public Integer getNatural() {
        return natural;
    }

    public String getOrigin() {
        return origin;
    }

    public Integer getRare() {
        return rare;
    }

    public Integer getRex() {
        return rex;
    }

    public Integer getShedding_level() {
        return shedding_level;
    }

    public Integer getShort_legs() {
        return short_legs;
    }

    public Integer getSocial_needs() {
        return social_needs;
    }

    public Integer getStranger_friendly() {
        return stranger_friendly;
    }

    public Integer getSuppressed_tail() {
        return suppressed_tail;
    }

    public String getTemperament() {
        return temperament;
    }

    public String getVcahospitals_url() {
        return vcahospitals_url;
    }

    public String getVetstreet_url() {
        return vetstreet_url;
    }

    public Integer getVocalisation() {
        return vocalisation;
    }

    public String getWikipedia_url() {
        return wikipedia_url;
    }
}
