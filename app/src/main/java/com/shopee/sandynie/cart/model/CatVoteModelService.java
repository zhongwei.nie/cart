package com.shopee.sandynie.cart.model;

import com.shopee.sandynie.cart.base.BaseModelService;
import com.shopee.sandynie.cart.config.AppConfig;
import com.shopee.sandynie.cart.paramter.CatVoteParamter;
import com.shopee.sandynie.cart.util.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;

public class CatVoteModelService extends BaseModelService {

    private static CatVoteModelService mInstance = new CatVoteModelService();

    private CatVoteModelService(){

    }
    public static CatVoteModelService getInstance(){
        return mInstance;
    }


    public Observable<List<CatVote>> getVoteRandomImage(CatVoteParamter paramter) {
        buildConnection(AppConfig.CATVOTELOADURL);
        return mRetrofit.create(CatVoteOperation.class).
                getRandomVoteImage();
    }


    public Observable<CatVoteResponse> sendCatVote(CatVoteParamter paramter){
        buildConnection(AppConfig.CATVOTESENDURL);

        JSONObject requestData = new JSONObject();
        try {
            requestData.put("image_id", paramter.getImageId());
            if(!StringUtils.isNullOrEmpty(paramter.getSubId()))
                requestData.put("sub_id", paramter.getSubId());
            requestData.put("value", paramter.getValue());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),requestData.toString());

        return mRetrofit.create(CatVoteOperation.class).sendVoteRequest(requestBody);
    }
}
