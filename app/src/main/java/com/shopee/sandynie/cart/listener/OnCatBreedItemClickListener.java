package com.shopee.sandynie.cart.listener;

import android.view.View;

import com.shopee.sandynie.cart.model.CatBreed;

public interface OnCatBreedItemClickListener {

    void OnItemClick(View view, int pos, CatBreed data);
}
