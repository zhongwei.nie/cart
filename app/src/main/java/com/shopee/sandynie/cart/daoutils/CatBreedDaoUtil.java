package com.shopee.sandynie.cart.daoutils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shopee.sandynie.cart.application.CatApp;
import com.shopee.sandynie.cart.daoentity.CatBreedEntity;
import com.shopee.sandynie.cart.greendao.CatBreedEntityDao;
import com.shopee.sandynie.cart.greendao.DaoSession;
import com.shopee.sandynie.cart.model.CatBreed;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CatBreedDaoUtil {

    protected static Gson gson;

    static {
        gson = new Gson();
    }


    public static void saveCatBreedToDao(long page, List<CatBreed> data) {
        String str = gson.toJson(data);
        CatBreedEntity entity = new CatBreedEntity(page, str);
        DaoSession daoSession = CatApp.getInstance().getDaoSession();
        CatBreedEntityDao dao = daoSession.getCatBreedEntityDao();
        dao.insert(entity);
    }

    public static List<CatBreed> getCatBreedsByPage(long page) {
        DaoSession daoSession = CatApp.getInstance().getDaoSession();
        CatBreedEntityDao dao = daoSession.getCatBreedEntityDao();
        CatBreedEntity entity = dao.load(page);
        if (entity != null) {
            Type founderListType = new TypeToken<ArrayList<CatBreed>>() {
            }.getType();

            return gson.fromJson(entity.getCatBreedListData(), founderListType);
        }
        return null;
    }

}
