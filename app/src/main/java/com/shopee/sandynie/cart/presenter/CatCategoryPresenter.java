package com.shopee.sandynie.cart.presenter;

import android.util.Log;

import com.shopee.sandynie.cart.base.BasePresenter;
import com.shopee.sandynie.cart.config.ModelEvent;
import com.shopee.sandynie.cart.model.CatCategory;
import com.shopee.sandynie.cart.model.CatCategoryModelService;
import com.shopee.sandynie.cart.paramter.CatCategoryParamter;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CatCategoryPresenter extends BasePresenter {

    private static final String TAG = CatCategoryPresenter.class.getSimpleName();

    private CatCategoryParamter paramter = new CatCategoryParamter();


    public void fetchData() {
        mView.showProgress();
        CatCategoryModelService.getInstance().getCatCategories(paramter)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<List<CatCategory>>() {
                @Override
                public void onCompleted() {
                    mView.dismissProgress();
                }

                @Override
                public void onError(Throwable e) {
                    mView.dismissProgress();
                    Log.d(TAG, "ON ERROR");
                }

                @Override
                public void onNext(List<CatCategory> obj) {

                    mView.updateView(obj, ModelEvent.FETCHCATCATEGORY);
                }
            });
    }
}
