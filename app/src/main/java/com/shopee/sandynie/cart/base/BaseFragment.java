package com.shopee.sandynie.cart.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.shopee.sandynie.cart.config.AppConstant;
import com.shopee.sandynie.cart.config.ModelEvent;

import butterknife.ButterKnife;

public class BaseFragment extends Fragment implements BaseView {

    ProgressBar spinner;

    @Override
    public void showProgress() {
        if (spinner != null)
            spinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgress() {
        if (spinner != null)
            spinner.setVisibility(View.GONE);
    }

    @Override
    public void showTips(String content) {
        Toast.makeText(getActivity(), content, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateView(Object data, ModelEvent eventTypes) {

    }


    @Override
    public void initData() {
    }


    @Nullable
    public <K extends View> K findViewById(@IdRes int id) {
        return getActivity().findViewById(id);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(spinner != null)
            dismissProgress();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        int resId  = bundle.getInt(AppConstant.ARFUMENT_RESID);
        View view = inflater.inflate(resId, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        spinner = getActivity().findViewById(AppConstant.LOADING_RESID);
        initData();
    }

    @Override
    public void forceUpdate() {

    }

    public int getIndex(){
        return 0;
    }

    @Override
    public Context getContext(){
        return getActivity();
    }

}
