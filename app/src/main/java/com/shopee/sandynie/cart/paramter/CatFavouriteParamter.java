package com.shopee.sandynie.cart.paramter;


public class CatFavouriteParamter extends BaseParamter {

    private int page = 0;
    private int limit = 10;
    private String subId;

    private int favouriteId;
    private int deleteIndex;

    public void setDeleteIndex(int deleteIndex) {
        this.deleteIndex = deleteIndex;
    }

    public int getDeleteIndex() {
        return deleteIndex;
    }

    public void setFavouriteId(int favouriteId) {
        this.favouriteId = favouriteId;
    }

    public int getFavouriteId() {
        return favouriteId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public  boolean canNext(){
        return true;
    }

    public boolean canPre(){
        return page >0;
    }

    public void nextPage(){
        page++;
    }

    public void prePage(){
        page--;
    }
}
