package com.shopee.sandynie.cart.paramter;

public class CatVoteParamter extends BaseParamter {

    private String imageId;
    private String subId;
    private int value;

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getImageId() {
        return imageId;
    }

    public String getSubId() {
        return subId;
    }

    public int getValue() {
        return value;
    }

    public void love(String imageId){
        this.imageId = imageId;
        value = 1;
    }

    public void nope(String imageId){
        this.imageId = imageId ;
        value = 0;
    }

    public void love(String imageId, String subId){
        this.imageId = imageId;
        this.subId = subId;
        value = 1;
    }

    public void nope(String imageId,String subId){
        this.imageId = imageId;
        this.subId = subId;
        value = 0;
    }

    public void set(String imageId,boolean love,String subId){
        this.imageId = imageId;
        this.subId = subId;
        value = love ? 1:0;
    }
}
