package com.shopee.sandynie.cart.model;

import com.shopee.sandynie.cart.config.AppConfig;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;

public interface CatCategoryOperation {

    @Headers(AppConfig.CATAPIKEY)
    @GET(AppConfig.LISTCATCATEGORYURL)
    Observable<List<CatCategory>> getCatgories(@Query("page") int page,
                                                @Query("limit") int limit);
}
