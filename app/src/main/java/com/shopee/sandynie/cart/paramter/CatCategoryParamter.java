package com.shopee.sandynie.cart.paramter;

public class CatCategoryParamter extends BaseParamter {
    public int getmPage() {
        return mPage;
    }

    public int getmLimit() {
        return mLimit;
    }

    private int mPage = 0;

    public void setmLimit(int mLimit) {
        this.mLimit = mLimit;
    }

    private int mLimit = 0;
}
