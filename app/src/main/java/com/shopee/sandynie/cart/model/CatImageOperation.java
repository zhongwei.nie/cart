package com.shopee.sandynie.cart.model;

import com.shopee.sandynie.cart.config.AppConfig;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface CatImageOperation {

    @Headers(AppConfig.CATAPIKEY)
    @GET(AppConfig.LISTALLIMAGEURL)
    Observable<List<CatImage>> getPublicImage(@Query("size") String size,
                                              @Query("mime_types") String mime_types,
                                              @Query("order") String order,
                                              @Query("limit") int limit,
                                              @Query("page") int page,
                                              @Query("category_ids") String category_ids,
                                              @Query("format") String format,
                                              @Query("breed_id") String breed_id
                                              );




    @Headers(AppConfig.CATAPIKEY)
    @POST(AppConfig.CATFAVOURITEURL)
    Observable<CatImageFavouriteResponse> sentFarvourite(@Body RequestBody requestBody);
}
