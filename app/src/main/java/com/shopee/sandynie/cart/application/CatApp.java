package com.shopee.sandynie.cart.application;

import android.app.Application;

import com.shopee.sandynie.cart.config.AppConfig;
import com.shopee.sandynie.cart.greendao.DaoMaster;
import com.shopee.sandynie.cart.greendao.DaoSession;

import org.greenrobot.greendao.database.Database;


public class CatApp extends Application {

    private static final String TAG = CatApp.class.getSimpleName();

    private DaoSession daoSession;
    private static CatApp instance;


    public static CatApp getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // regular SQLite database
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, AppConfig.DBNAME);
        Database db = helper.getWritableDb();

        // encrypted SQLCipher database
        // note: you need to add SQLCipher to your dependencies, check the build.gradle file
        // DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "notes-db-encrypted");
        // Database db = helper.getEncryptedWritableDb("encryption-key");

        daoSession = new DaoMaster(db).newSession();
        instance = this;
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
