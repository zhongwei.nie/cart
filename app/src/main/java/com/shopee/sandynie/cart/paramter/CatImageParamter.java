package com.shopee.sandynie.cart.paramter;

public class CatImageParamter extends BaseParamter{

    private String size = "small";

    private String mime_types = "";

    private String order = "Rand";
    private int limit = 3;
    private int page = 0;
    private String category_ids = "";
    private String format = "json";
    private String breed_id = "";
    private String favouriteId;

    public void setId(String id) {
        this.favouriteId = id;
    }

    public String getId() {
        return favouriteId;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMime_types() {
        return mime_types;
    }

    public void setMime_types(String mime_types) {
        this.mime_types = mime_types;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getCategory_ids() {
        return category_ids;
    }

    public void setCategory_ids(String category_ids) {
        this.category_ids = category_ids;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getBreed_id() {
        return breed_id;
    }

    public void setBreed_id(String breed_id) {
        this.breed_id = breed_id;
    }

    public boolean canNext(){
        return true;
    }

    public boolean canPre(){
        return page >0;
    }

    public void nextPage(){
        page++;
    }

    public void prePage(){
        page--;
    }


}
