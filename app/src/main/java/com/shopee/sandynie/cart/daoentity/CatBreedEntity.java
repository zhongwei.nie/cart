package com.shopee.sandynie.cart.daoentity;

import com.shopee.sandynie.cart.model.CatBreed;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.List;

import org.greenrobot.greendao.annotation.Generated;

@Entity
public class CatBreedEntity {
    @Id(autoincrement = false)
    private Long page;

    private String catBreedListData;

    @Generated(hash = 1410243988)
    public CatBreedEntity(Long page, String catBreedListData) {
        this.page = page;
        this.catBreedListData = catBreedListData;
    }

    @Generated(hash = 1823850260)
    public CatBreedEntity() {
    }

    public Long getPage() {
        return this.page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public String getCatBreedListData() {
        return this.catBreedListData;
    }

    public void setCatBreedListData(String catBreedListData) {
        this.catBreedListData = catBreedListData;
    }

}
