package com.shopee.sandynie.cart.base;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.shopee.sandynie.cart.R;
import com.shopee.sandynie.cart.config.AppConstant;
import com.shopee.sandynie.cart.config.ModelEvent;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseAcitivity extends AppCompatActivity implements BaseView {

    @BindView(AppConstant.LOADING_RESID)
    ProgressBar spinner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
        ButterKnife.bind(this);
    }

    @Override
    public void showProgress() {

        if (spinner != null)
            spinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgress() {

        if (spinner != null)
            spinner.setVisibility(View.GONE);
    }

    @Override
    public void showTips(String content) {
        Toast ss = Toast.makeText(this, content,Toast.LENGTH_SHORT);
        ss.show() ;
    }

    @Override
    public void updateView(Object data, ModelEvent eventType) {

    }


    @Override
    public void initData() {

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void forceUpdate() {

    }
}
